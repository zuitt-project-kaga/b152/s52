let collection = [];

//Write the queue functions below.
//NOTE: DO NOT USE ANY ARRAY METHODS. YOU CAN USE .length property

// List of array methods that you SHOULD NOT use:
// concat(), copyWithin(), entries(), every(), fill(), filter(), find(), findIndex(), from(), includes(), indexOf(), isArray(), join(), keys(), lastIndexOf(), map(), pop(), push(), reduce(), reduceRight(), reverse(), shift(), slice(), some(), sort(), splice(), toString(), unshift(), valueOf()

//Exception: forEach()
//NOTE: USE return KEYWORD AND NOT console.log() FOR RETURNING VALUES

function print(){

	// Return the collection array.
	return collection;
}


function enqueue(element){

	// Add element at the end of the queue without the use of array methods.
	// Return the collection array
	collection = [...collection,element];
	return collection;

}


function dequeue(){

	// Remove the first element of the collection array without the use of array methods.
	// Return the updated/manipulated collection array
	let newArray = [];
	let i = 0;
	
	collection.forEach(item => {

		if(i >= 1){
			newArray[i - 1] = item
		}
		i++
	})

	collection = [...newArray]
	return collection;
	
}

function front(){

	//Return the first item in the collection array
	return collection[0];
}

function size(){

	//return the current number of items in the collection array.
	return collection.length;
}

function isEmpty(){

	//Check if the array is empty or not and return a boolean.
	//return true if the collection array is empty.
	//return false if it is not.
	return collection.length === 0;
}



module.exports = {

print,
enqueue,
dequeue,
front,
size,
isEmpty

};