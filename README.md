To start using the built-in tests: 
run <code>npm install</code> first then issue the <code>npm test</code> at the root directory of this project.

WARNING: Do not change any code inside <code>test.js</code>.

## Pushing Instructions

### Go to Gitlab:
	-in your zuitt-projects folder and access b123 folder.
	-inside your b123 folder create a new repo called s54
	-untick the readme option
	-copy the git url from the clone button of your s54 repo.

### Delete the .git folder for your local s54-activity.

### Initialize a new git in the s54-activity folder:
	git init
### Add your updates: 
	git add .
### Commit your changes: 
	git commit -m "includes solution Data Structure Mock Technical Exam".
### Connect your remote s54 repo to this local repo: 
	git remote add origin <url>
### Push your updates:
	git push origin master

### Link your s54e repo to boodle:
	WD078-54 | Mock Technical Exam (Data Structures and Algorithms)